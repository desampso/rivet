import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections =["/afs/cern.ch/user/d/desampso/3rivet/mc15_13TeV.364499.MGPy8EG_NNPDF30LO_A14NNPDF23LO_WZlvlljjEWK.evgen
.EVNT.e6376/EVNT.12632258._000001.pool.root.1"]
#svcMgr.EventSelector.InputCollections =["EVNT.06628325._000130.pool.root.1"]
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()
from AthenaCommon.AppMgr import (theApp, ServiceMgr as svcMgr,ToolSvc)
svcMgr.MessageSvc.debugLimit = 5000000
from Rivet_i.Rivet_iConf import Rivet_i
theApp.EvtMax = -1
from AthenaServices.AthenaServicesConf import AthenaEventLoopMgr
svcMgr += AthenaEventLoopMgr()
svcMgr.AthenaEventLoopMgr.EventPrintoutInterval = 200
rivet = Rivet_i()
Rivet_i.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'RivetATLAS_2019_I1720438.cc' ]
rivet.RunName = ""
rivet.HistoFile = "myanalysis_output"
job += rivet

